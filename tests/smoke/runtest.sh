#!/bin/bash

## Get to the test directory
cd ../source/

#build the source tree to get the tests
echo "Building test out of $(pwd):"
./autogen
./configure
make

#go to the test directory
cd test
make get_module
make get_device

#run the get_module test on every loaded module, if any command exits non-zero,
#fail the test
for i in `awk '{print $1}' /proc/modules`
do
	./get_module $i
	if [ $? -ne 0 ]
	then
		echo "GET MODULE FAILED ON $i"
		exit 1
	fi
done

#run the get_device test on pci and usb busses
for i in `ls /sys/bus/pci/devices`
do
	./get_device pci $i
	if [ $? -ne 0 ]
	then
		echo "GET DEVICE FAILED ON PCI DEV $i"
		exit 1
	fi
done

for i in `ls /sys/bus/usb/devices`
do
	./get_device usb $i
	if [ $? -ne 0 ]
	then
		echo "GET DEVICE FAILED ON USB DEV $i"
		exit 1
	fi
done

#If we get here all tests passed, exit 0
exit 0
